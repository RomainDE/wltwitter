package worldline.ssm.rd.ux.wltwitter.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterActivity;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;

/**
 * Created by elerion on 10/28/15.
 */
public class Notifications {

    public static void displayNewTweetsNotification(int nb, boolean vibrate){
        final Context context = WLTwitterApplication.getContext();

        String nbTweets = "Number of tweet : " + Integer.toString(nb);

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.social136)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(nbTweets)
                .setAutoCancel(true);

        final Intent intent = new Intent(context, WLTwitterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        final TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(WLTwitterActivity.class);
        stackBuilder.addNextIntent(intent);

        final PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);

        Notification notification = builder.build();

        if(vibrate){
            notification.defaults = Notification.DEFAULT_VIBRATE;
        }

        final NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(42, notification);

    }
}
