package worldline.ssm.rd.ux.wltwitter.fragments.tweets.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import worldline.ssm.rd.ux.wltwitter.R;

/**
 * Created by romain on 09/10/2015.
 */
public class SingleTweetViewHolder {

    public ImageView tweetSingleImage;
    public TextView tweetSingleUsername, tweetSingleAlias, tweetSingleContent;

    public SingleTweetViewHolder(View view) {
        tweetSingleImage = (ImageView) view.findViewById(R.id.tweet_single_tweet_image);
        tweetSingleUsername = (TextView) view.findViewById(R.id.tweet_single_username);
        tweetSingleAlias = (TextView) view.findViewById(R.id.tweet_single_alias);
        tweetSingleContent = (TextView) view.findViewById(R.id.tweet_single_content);
    }
}
