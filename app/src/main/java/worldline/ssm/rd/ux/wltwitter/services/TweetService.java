package worldline.ssm.rd.ux.wltwitter.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.asynctweets.TwitterAsyncTask;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.listeners.TweetListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

/**
 * Created by Quentin on 23/10/2015.
 */

public class TweetService extends Service implements TweetListener {
    public final IBinder mBinder = new WLTwitterBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null){
            Bundle b = intent.getExtras();
            String login = b.getString("login");
            new TwitterAsyncTask(this).execute(login);
        }
        Log.i("TweetService", "Service start");
        return Service.START_NOT_STICKY;
    }


    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public void onTweetsRetrieved(List<Tweet> tweets) {
//        //inserer tweets en dv
        WLTwitterDatabaseManager.testContentProvider(tweets);

        int nb = 0;
        nb = tweets.size();

        if(nb > 0){
            final Intent intent = new Intent(Constants.General.NEW_TWEET);
            final Bundle extras = new Bundle();
            extras.putInt(Constants.General.Nb_NEW_TWEETS, nb);
            intent.putExtras(extras);
            sendBroadcast(intent);
        }

        //stop service
        this.stopSelf();
    }

    public class WLTwitterBinder extends Binder {
        public TweetService getService(){
            return TweetService.this;
        }
    }
}

