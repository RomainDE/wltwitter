package worldline.ssm.rd.ux.wltwitter.fragments.tweets;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.LoaderManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Calendar;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.adapter.TweetsCursorAdapter;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseContract;
import worldline.ssm.rd.ux.wltwitter.listeners.ClickListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.services.TweetService;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

/**
 * Created by elerion on 10/6/15.
 */
public class TweetsFragments extends Fragment implements AdapterView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, LoaderManager.LoaderCallbacks<Cursor> {

    private SwipeRefreshLayout root;
    private RecyclerView tweetsView;

    private String login = "";

    private Activity activity;

    private TweetsCursorAdapter tweetsCursorAdapter;

    private ClickListener clickListener;

    public static TweetsFragments newInstance(String log) {
        TweetsFragments fragment = new TweetsFragments();
        fragment.login = log;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.activity = getActivity();
        this.root = (SwipeRefreshLayout) inflater.inflate(R.layout.fragment_tweets, container, false);
        this.tweetsView = (RecyclerView) this.root.findViewById(R.id.tweetsListView);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(WLTwitterApplication.getContext());
        this.tweetsView.setLayoutManager(layoutManager);
        this.root.setOnRefreshListener(this);
        return this.root;
    }

    @Override
    public void onStart() {
        super.onStart();

        if(TextUtils.isEmpty(this.login)){
            Toast.makeText(WLTwitterApplication.getContext(), "ERROR login is empty", Toast.LENGTH_SHORT);
        }else{
            /*
        Ajout d'une progresse bar
         */
            final ProgressBar progressBar = new ProgressBar(this.activity);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            progressBar.setLayoutParams(layoutParams);
            progressBar.setIndeterminate(true);
            ((ViewGroup) this.root).addView(progressBar);

            //new TwitterAsyncTask(this).execute(this.login);

            getLoaderManager().initLoader(0, null, this);
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.clickListener = (ClickListener) activity;
        }catch (ClassCastException e){
            throw new  ClassCastException(activity.toString() + "OnTweetCliked not implement");
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Tweet tweet = (Tweet) parent.getItemAtPosition(position);
        clickListener.onTweetClicked(tweet);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final CursorLoader cl = new CursorLoader(WLTwitterApplication.getContext());
        cl.setUri(WLTwitterDatabaseContract.TWEETS_URI);
        cl.setProjection(WLTwitterDatabaseContract.PROJECTION_FULL);
        cl.setSelection(null);
        cl.setSelectionArgs(null);
        cl.setSortOrder(null);
        return cl;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data != null){

            this.tweetsCursorAdapter = new TweetsCursorAdapter(WLTwitterApplication.getContext(), data, (ClickListener) this.activity);
            this.tweetsView.setAdapter(this.tweetsCursorAdapter);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onRefresh() {
    }
}
