package worldline.ssm.rd.ux.wltwitter;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Toast;

import java.util.Calendar;

import worldline.ssm.rd.ux.wltwitter.listeners.ClickListener;
import worldline.ssm.rd.ux.wltwitter.listeners.RetweetListener;
import worldline.ssm.rd.ux.wltwitter.listeners.ImageClickListener;
import worldline.ssm.rd.ux.wltwitter.fragments.tweets.FullTweetImage;
import worldline.ssm.rd.ux.wltwitter.fragments.tweets.TweetFragment;
import worldline.ssm.rd.ux.wltwitter.fragments.tweets.TweetsFragments;
import worldline.ssm.rd.ux.wltwitter.helpers.UserHelper;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.receivers.TweetsReceiver;
import worldline.ssm.rd.ux.wltwitter.services.TweetService;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

public class WLTwitterActivity extends Activity implements ClickListener, RetweetListener, ImageClickListener {

    private Context wContext;

    private String userLogin = "";

    private TweetsFragments tweetsFragments;

    private TweetsReceiver tweetsReceiver;

    private TweetService myService;

    private PendingIntent mServicePendingIntent;

    public ServiceConnection myConnection = new ServiceConnection(){
            public void onServiceConnected(ComponentName className, IBinder binder){
                myService = ((TweetService.WLTwitterBinder) binder).getService();
            }

            public void onServiceDisconnected(ComponentName className){
                myService = null;
            }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wltwitter);
        wContext = getApplicationContext();

        this.userLogin = UserHelper.getCurrentUser(wContext);

        getActionBar().setTitle(getString(R.string.app_name) + " - " + this.userLogin);

        tweetsFragments = TweetsFragments.newInstance(this.userLogin);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.wl_twitter, tweetsFragments);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wltwitter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.actionLogout:
                UserHelper.deleteCurrentUser(wContext);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTweetClicked(Tweet tweet) {
        TweetFragment tweetFragment = TweetFragment.newInstance(tweet, (ImageClickListener) this, (RetweetListener) this);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.wl_twitter, tweetFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager.getBackStackEntryCount() > 0){
            fragmentManager.popBackStack();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public void onImageClick(String url) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FullTweetImage fullTweetImage = FullTweetImage.newInstance(url);
        fragmentTransaction.replace(R.id.wl_twitter, fullTweetImage);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onRetweet(Tweet tweet) {
        Toast.makeText(wContext, tweet.text, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        tweetsReceiver = new TweetsReceiver();
        registerReceiver(tweetsReceiver, new IntentFilter(Constants.General.NEW_TWEET));

        if(myService == null){
            Intent intent = null;
            intent = new Intent(this, TweetService.class);
            bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
        }

        final Calendar cal = Calendar.getInstance();
        final Intent serviceIntent = new Intent(this, TweetService.class);
        Bundle b = new Bundle();
        b.putString("login", this.userLogin);
        serviceIntent.putExtras(b);
        mServicePendingIntent = PendingIntent.getService(getApplicationContext(), 0, serviceIntent, 0);
        final AlarmManager am =  (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), Constants.Twitter.POLLING_DELAY, mServicePendingIntent);

        super.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(tweetsReceiver);
        tweetsReceiver = null;

        if(myService != null){
            unbindService(myConnection);
            myService = null;
        }
        final AlarmManager am =  (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        am.cancel(mServicePendingIntent);

        super.onPause();
    }
}
