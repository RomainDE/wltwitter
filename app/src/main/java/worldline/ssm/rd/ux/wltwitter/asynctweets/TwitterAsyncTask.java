package worldline.ssm.rd.ux.wltwitter.asynctweets;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;


import worldline.ssm.rd.ux.wltwitter.listeners.TweetListener;
import worldline.ssm.rd.ux.wltwitter.helpers.TwitterHelper;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by elerion on 02/10/15.
 */
public class TwitterAsyncTask extends AsyncTask<String, Integer, List<Tweet>> {

    TweetListener listener;

    public TwitterAsyncTask(TweetListener listener) {
        this.listener = listener;
    }

    @Override
    protected List<Tweet> doInBackground(String... params) {
        String login = params[0];
        if(TextUtils.isEmpty(login)){
            return null;
        }
        return TwitterHelper.getTweetsOfUser(login);
    }

    @Override
    protected void onPostExecute(List<Tweet> tweets) {
        if(tweets != null){
            for (Tweet tweet : tweets) {
                Log.i(this.getClass().getName(), tweet.text);
            }
            super.onPostExecute(tweets);
            listener.onTweetsRetrieved(tweets);
        }
    }
}
