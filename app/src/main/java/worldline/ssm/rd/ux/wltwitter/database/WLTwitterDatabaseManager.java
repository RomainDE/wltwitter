package worldline.ssm.rd.ux.wltwitter.database;

import java.util.Date;
import java.util.List;

import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;
import worldline.ssm.rd.ux.wltwitter.pojo.TwitterUser;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class WLTwitterDatabaseManager {

	public static Tweet tweetFromCursor(Cursor c){
		if (null != c){
			final Tweet tweet = new Tweet();
			tweet.user = new TwitterUser();

			// Retrieve the date created
			if (c.getColumnIndex(WLTwitterDatabaseContract.DATE_CREATED) >= 0){
				tweet.dateCreated = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.DATE_CREATED));
			}

			// Retrieve the user name
			if (c.getColumnIndex(WLTwitterDatabaseContract.USER_NAME) >= 0){
				tweet.user.name = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.USER_NAME));
			}

			// Retrieve the user alias
			if (c.getColumnIndex(WLTwitterDatabaseContract.USER_ALIAS) >= 0){
				tweet.user.screenName = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.USER_ALIAS));
			}

			// Retrieve the user image url
			if (c.getColumnIndex(WLTwitterDatabaseContract.USER_IMAGE_URL) >= 0){
				tweet.user.profileImageUrl = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.USER_IMAGE_URL));
			}

			// Retrieve the text of the tweet
			if (c.getColumnIndex(WLTwitterDatabaseContract.TEXT) >= 0){
				tweet.text = c.getString(c.getColumnIndex(WLTwitterDatabaseContract.TEXT));
			}

			return tweet;
		}
		return null;
	}

	public static ContentValues tweetToContentValues(Tweet tweet){
		final ContentValues values = new ContentValues();

		// Set the date created
		if (!TextUtils.isEmpty(tweet.dateCreated)){
			values.put(WLTwitterDatabaseContract.DATE_CREATED, tweet.dateCreated);
		}

		// Set the date created as timestamp
		values.put(WLTwitterDatabaseContract.DATE_CREATED_TIMESTAMP, tweet.getDateCreatedTimestamp());

		// Set the user name
		if (!TextUtils.isEmpty(tweet.user.name)){
			values.put(WLTwitterDatabaseContract.USER_NAME, tweet.user.name);
		}

		// Set the user alias
		if (!TextUtils.isEmpty(tweet.user.screenName)){
			values.put(WLTwitterDatabaseContract.USER_ALIAS, tweet.user.screenName);
		}

		// Set the user image url
		if (!TextUtils.isEmpty(tweet.user.profileImageUrl)){
			values.put(WLTwitterDatabaseContract.USER_IMAGE_URL, tweet.user.profileImageUrl);
		}

		// Set the text of the tweet
		if (!TextUtils.isEmpty(tweet.text)){
			values.put(WLTwitterDatabaseContract.TEXT, tweet.text);
		}

		return values;
	}

	public static void testDatabase(List<Tweet> tweets){
		// TODO Retrieve a writableDatabase from your database helper
		final SQLiteOpenHelper sqlOpenHelper = new WLTwitterDatabaseHelper(WLTwitterApplication.getContext());
		final SQLiteDatabase tweetsDB = sqlOpenHelper.getWritableDatabase();

		// TODO Then iterate over the list of tweets, and insert all tweets in database
		for (Tweet t : tweets){
			tweetsDB.insert(WLTwitterDatabaseContract.TABLE_TWEETS, "", tweetToContentValues(t));
		}

		// TODO Finally, after inserting all tweets in database, query the database to retrieve all entries as cursor, and log
		final Cursor cursor = tweetsDB.query(WLTwitterDatabaseContract.TABLE_TWEETS, WLTwitterDatabaseContract.PROJECTION_FULL, null, null, null, null, null);
		while(cursor.moveToNext()){
			Tweet t = tweetFromCursor(cursor);
			Log.i("DB", t.toString());
		}
		if(!cursor.isClosed()){
			cursor.close();
		}

	}

	public static void testContentProvider(List<Tweet> tweets){
		// TODO Test your ContentProvider here
		ContentResolver resolver = WLTwitterApplication.getContext().getContentResolver();

		//Insert Tweet
		for (Tweet tweet : tweets) {
			resolver.insert(WLTwitterDatabaseContract.TWEETS_URI, WLTwitterDatabaseManager.tweetToContentValues(tweet));
		}

		//Insert tweets
		Tweet t1 = createTweet("Doc", "delorean85", "En route Marty");
		Tweet t2 = createTweet("Marty McFly","mcfly","Retour vers le futur");
		Uri tweet_URI_1 = resolver.insert(WLTwitterDatabaseContract.TWEETS_URI, tweetToContentValues(t1));
		Uri tweet_URI_2 = resolver.insert(WLTwitterDatabaseContract.TWEETS_URI, tweetToContentValues(t2));

		//Update tweet
		String idTweet1 = String.valueOf(ContentUris.parseId(tweet_URI_1));

		ContentValues cv1 = new ContentValues();
		cv1.put(WLTwitterDatabaseContract.USER_NAME, "Dc Brown");
		cv1.put(WLTwitterDatabaseContract.TEXT, "Mais le futur c'est maintenant");
		resolver.update(WLTwitterDatabaseContract.TWEETS_URI, cv1, WLTwitterDatabaseContract.SELECTION_BY_ID, new String[]{idTweet1});

		String idTweet2 = String.valueOf(ContentUris.parseId(tweet_URI_2));
		ContentValues cv2 = new ContentValues();
		cv2.put(WLTwitterDatabaseContract.TEXT, "Retour vers le présent");
		resolver.update(WLTwitterDatabaseContract.TWEETS_URI, cv2, WLTwitterDatabaseContract.SELECTION_BY_ID, new String[]{idTweet2});

		//Delete tweet
		resolver.delete(WLTwitterDatabaseContract.TWEETS_URI, WLTwitterDatabaseContract.SELECTION_BY_ID, new String[]{idTweet1});
	}

	private static Tweet createTweet(String username, String alias, String content){
		final Tweet tweet = new Tweet();
		tweet.user = new TwitterUser();
		tweet.user.name = username;
		tweet.user.screenName = alias;
		tweet.text = content;
		tweet.user.profileImageUrl = "url";
		tweet.dateCreated = new Date().toString();
		return tweet;
	}
}
