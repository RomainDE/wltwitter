package worldline.ssm.rd.ux.wltwitter.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseHelper;
import worldline.ssm.rd.ux.wltwitter.database.WLTwitterDatabaseManager;
import worldline.ssm.rd.ux.wltwitter.fragments.tweets.viewholder.TweetsViewHolder;
import worldline.ssm.rd.ux.wltwitter.listeners.ClickListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by romain on 23/10/2015.
 */
public class TweetsCursorAdapter extends CursorRecyclerViewAdapter {

    ClickListener clickListener;

    public TweetsCursorAdapter(Context context, Cursor cursor, ClickListener listener) {
        super(context, cursor);
        this.clickListener = listener;
    }


    @Override
    public TweetsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_tweets, parent, false);
        return new TweetsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, Cursor cursor) {

        TweetsViewHolder holder = (TweetsViewHolder) viewHolder;
        final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);
        Log.d("TweetAdapter", tweet.toString());
        holder.setListener(tweet, this.clickListener);
        if (tweet != null) {
            holder.tweetUserName.setText(tweet.user.name);
            holder.tweetAlias.setText("@" + tweet.user.screenName);
            holder.tweetContent.setText(tweet.text);
            if (!TextUtils.isEmpty(tweet.user.profileImageUrl))
                Picasso.with(WLTwitterApplication.getContext()).load(tweet.user.profileImageUrl).into(holder.tweetImage);
        }

    }
}
