package worldline.ssm.rd.ux.wltwitter.fragments.tweets;

import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import worldline.ssm.rd.ux.wltwitter.listeners.RetweetListener;
import worldline.ssm.rd.ux.wltwitter.listeners.ImageClickListener;
import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link TweetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TweetFragment extends Fragment implements View.OnClickListener{
    Tweet tweet;

    View root;

    ImageClickListener imageClickListener;
    RetweetListener retweetListener;
    public static TweetFragment newInstance(Tweet tweet, ImageClickListener imageClickListener, RetweetListener retweetListener) {
        TweetFragment fragment = new TweetFragment();
        fragment.tweet = tweet;
        fragment.imageClickListener = imageClickListener;
        fragment.retweetListener = retweetListener;
        return fragment;
    }

    public TweetFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_tweet, container, false);
        this.load();
        return root;
    }

    private void load(){
        ((TextView) this.root.findViewById(R.id.tweet_single_username)).setText(tweet.user.name);
        ((TextView) this.root.findViewById(R.id.tweet_single_alias)).setText("@" + tweet.user.screenName);
        ((TextView) this.root.findViewById(R.id.tweet_single_content)).setText(tweet.text);
        Button b = ((Button) this.root.findViewById(R.id.tweet_single_button_retweet));
        b.setOnClickListener(this);
        ImageView imageView = ((ImageView) this.root.findViewById(R.id.tweet_single_tweet_image));
        imageView.setTag(tweet.user.profileImageUrl);
        if(!TextUtils.isEmpty(tweet.user.profileImageUrl)){
            Picasso.with(WLTwitterApplication.getContext()).load(tweet.user.profileImageUrl).resize(100,100).into(imageView);
            imageView.setOnClickListener(this);
        }

    }


    @Override
    public void onClick(View v) {
        if(v instanceof ImageView){
            ImageView image = (ImageView) v;
            String url = (String) image.getTag();
            imageClickListener.onImageClick(url);
        }else if(v instanceof Button){
            Log.i("TweetFragment", "Button");
            retweetListener.onRetweet(tweet);
        }
    }
}
