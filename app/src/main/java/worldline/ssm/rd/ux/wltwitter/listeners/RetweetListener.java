package worldline.ssm.rd.ux.wltwitter.listeners;

import android.view.View;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by Quentin on 12/10/2015.
 */
public interface RetweetListener{
    public void onRetweet(Tweet tweet);
}
