package worldline.ssm.rd.ux.wltwitter.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.listeners.ClickListener;
import worldline.ssm.rd.ux.wltwitter.listeners.RetweetListener;
import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;
import worldline.ssm.rd.ux.wltwitter.fragments.tweets.viewholder.TweetsViewHolder;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by elerion on 10/6/15.
 */
public class TweetsAdapter extends RecyclerView.Adapter<TweetsViewHolder> {
    private List<Tweet> tweets;

    private TweetsViewHolder holder;
    private ClickListener clickListener;

    public TweetsAdapter(ClickListener listener, List<Tweet> t){
        super();
        this.tweets = t;
        this.clickListener = listener;
    }

    @Override
    public TweetsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_tweets, parent, false);
        return new TweetsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TweetsViewHolder holder, int position) {
        if(position < getItemCount()){
            final Tweet tweet = tweets.get(position);
            holder.setListener(tweet, this.clickListener);
            if(tweet != null){
                holder.tweetUserName.setText(tweet.user.name);
                holder.tweetAlias.setText("@" + tweet.user.screenName);
                holder.tweetContent.setText(tweet.text);
                if(!TextUtils.isEmpty(tweet.user.profileImageUrl))
                    Picasso.with(WLTwitterApplication.getContext()).load(tweet.user.profileImageUrl).into(holder.tweetImage);
            }
        }
    }

    @Override
    public int getItemCount() {
        if(tweets == null)
            return 0;

        return tweets.size();
    }
}
