package worldline.ssm.rd.ux.wltwitter.listeners;

import java.util.List;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by elerion on 02/10/15.
 */
public interface TweetListener {

    public void onTweetsRetrieved(List<Tweet> tweets);
}
