package worldline.ssm.rd.ux.wltwitter.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.EditText;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

/**
 * Created by romai on 26/09/2015.
 */
public class UserHelper {

    public static String checkLogin(Context contex, EditText userName, EditText password) {
        String error = "";

        if (TextUtils.isEmpty(userName.getText().toString())) {
            error = error + contex.getString(R.string.error_no_login);
        }
        if (TextUtils.isEmpty(password.getText().toString())) {
            error = error + System.getProperty("line.separator") + contex.getString(R.string.error_no_password);
        }

        return error;
    }

    public static Boolean storeUser(Context context,EditText userName, EditText password) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.Preferences.PREF_LOGIN, userName.getText().toString());
        editor.putString(Constants.Preferences.PREF_PASSWORD, password.getText().toString());
        editor.apply();
        return UserHelper.userIsLogIn(context);
    }

    public static Boolean userIsLogIn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        return !TextUtils.isEmpty(prefs.getString(Constants.Preferences.PREF_LOGIN, ""));
    }

    public static String getCurrentUser(Context context) {
        return context.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, context.MODE_PRIVATE).getString(Constants.Preferences.PREF_LOGIN, "");
    }

    public static  void deleteCurrentUser(Context context){
        context.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCES_FILE_NAME, context.MODE_PRIVATE).edit().remove(Constants.Preferences.PREF_LOGIN).apply();
    }

}
