package worldline.ssm.rd.ux.wltwitter.listeners;

/**
 * Created by elerion on 10/12/15.
 */
public interface ImageClickListener {

    public void onImageClick(String url);
}
