package worldline.ssm.rd.ux.wltwitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import worldline.ssm.rd.ux.wltwitter.helpers.UserHelper;
import worldline.ssm.rd.ux.wltwitter.utils.Constants;

public class WLTwitterLoginActivity extends Activity implements View.OnClickListener {

    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this.getApplicationContext();

        setContentView(R.layout.activity_wltwitter_login);
        findViewById(R.id.login_button).setOnClickListener(this);

        Log.i("info", UserHelper.userIsLogIn(mContext).toString());

        if (UserHelper.userIsLogIn(mContext)) {
            this.launchTwitter();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_wltwitter_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        EditText userName = (EditText) findViewById(R.id.login_field_username);
        EditText password = (EditText) findViewById(R.id.login_field_password);

        String error = UserHelper.checkLogin(mContext, userName, password);

        if (!TextUtils.isEmpty(error)) {
            Toast.makeText(mContext, error, Toast.LENGTH_SHORT).show();
        } else {
            if (UserHelper.storeUser(mContext, userName, password)) {

                userName.setText("");
                password.setText("");

                Log.i("info", UserHelper.userIsLogIn(mContext).toString());

                this.launchTwitter();
            } else {
                Toast.makeText(mContext, "Error during save your user name " + userName.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void launchTwitter() {
        Intent intent = new Intent(this, WLTwitterActivity.class);
        startActivity(intent);
    }
}
