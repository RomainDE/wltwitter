package worldline.ssm.rd.ux.wltwitter.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import worldline.ssm.rd.ux.wltwitter.utils.Constants;
import worldline.ssm.rd.ux.wltwitter.utils.Notifications;

/**
 * Created by elerion on 10/28/15.
 */
public class TweetsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        final int nb = intent.getExtras().getInt(Constants.General.Nb_NEW_TWEETS);

        Notifications.displayNewTweetsNotification(nb, true);

    }
}
