package worldline.ssm.rd.ux.wltwitter.fragments.tweets;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.WLTwitterApplication;

public class FullTweetImage extends Fragment {

    private String url;
    private View root;

    public static FullTweetImage newInstance(String param1) {
        FullTweetImage fragment = new FullTweetImage();
        fragment.url = param1;
        return fragment;
    }

    public FullTweetImage() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.root = inflater.inflate(R.layout.fragment_full_tweet_image, container, false);
        ImageView imageView = (ImageView) root.findViewById(R.id.full_image_tweet);
        Picasso.with(WLTwitterApplication.getContext()).load(this.url).into(imageView);
        return  root;
    }


}
