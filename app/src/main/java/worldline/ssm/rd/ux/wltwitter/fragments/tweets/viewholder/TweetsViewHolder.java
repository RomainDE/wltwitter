package worldline.ssm.rd.ux.wltwitter.fragments.tweets.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import worldline.ssm.rd.ux.wltwitter.listeners.ClickListener;
import worldline.ssm.rd.ux.wltwitter.R;
import worldline.ssm.rd.ux.wltwitter.listeners.RetweetListener;
import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by romain on 09/10/2015.
 */
public class TweetsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public ImageView tweetImage;
    public Button tweetRtButton;
    public TextView tweetUserName, tweetAlias, tweetContent;

    ClickListener clickListener;
    Tweet tweet;
    View vi;

    public TweetsViewHolder(View view) {
        super(view);
        vi = view;
        tweetImage = (ImageView) view.findViewById(R.id.tweet_image);
        tweetRtButton = (Button) view.findViewById(R.id.tweet_RTbutton);
        tweetUserName = (TextView) view.findViewById(R.id.tweet_username);
        tweetAlias = (TextView) view.findViewById(R.id.tweet_alias);
        tweetContent = (TextView) view.findViewById(R.id.tweet_content);

        tweetRtButton.setOnClickListener(this);
        vi.setOnClickListener(this);
    }

    public void setListener(Tweet tweet, ClickListener clickListener){
        this.clickListener = clickListener;
        this.tweet = tweet;

    }

    @Override
    public void onClick(View v) {
        if(v instanceof Button){
            RetweetListener retweetListener = (RetweetListener) this.clickListener;
            retweetListener.onRetweet(tweet);
        }else{
            clickListener.onTweetClicked(tweet);
        }
    }
}
