package worldline.ssm.rd.ux.wltwitter.listeners;

import worldline.ssm.rd.ux.wltwitter.pojo.Tweet;

/**
 * Created by elerion on 10/6/15.
 */
public interface ClickListener {
    public  void onTweetClicked(Tweet tweet);
}
